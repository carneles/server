# Building the app
FROM golang:1.20-bullseye as builder

WORKDIR /server

COPY . .
RUN make server_build

# Run the app
FROM debian:bullseye-slim

RUN mkdir /server

WORKDIR /server

EXPOSE 9090

COPY --from=builder /server/server /server

CMD /server/server
