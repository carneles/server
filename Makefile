lint:
	golangci-lint run

protoc:
	protoc --go_out=. \
		--go_opt=paths=source_relative \
		--go-grpc_out=. \
		--go-grpc_opt=require_unimplemented_servers=false,paths=source_relative \
		internal/summary/*.proto 

server_build:
	go build -o server apps/server/main.go

server_run:
	CONVERSION_VALUE=10 go run apps/server/main.go

client_build:
	go build -o client apps/client/main.go

