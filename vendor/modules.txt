# github.com/cespare/xxhash/v2 v2.2.0
## explicit; go 1.11
github.com/cespare/xxhash/v2
# github.com/cpuguy83/go-md2man/v2 v2.0.2
## explicit; go 1.11
github.com/cpuguy83/go-md2man/v2/md2man
# github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f
## explicit
github.com/dgryski/go-rendezvous
# github.com/go-redis/redismock/v9 v9.2.0
## explicit; go 1.18
github.com/go-redis/redismock/v9
# github.com/golang/protobuf v1.5.3
## explicit; go 1.9
github.com/golang/protobuf/jsonpb
github.com/golang/protobuf/proto
github.com/golang/protobuf/ptypes
github.com/golang/protobuf/ptypes/any
github.com/golang/protobuf/ptypes/duration
github.com/golang/protobuf/ptypes/timestamp
# github.com/klauspost/compress v1.15.9
## explicit; go 1.16
github.com/klauspost/compress
github.com/klauspost/compress/flate
github.com/klauspost/compress/fse
github.com/klauspost/compress/gzip
github.com/klauspost/compress/huff0
github.com/klauspost/compress/internal/cpuinfo
github.com/klauspost/compress/internal/snapref
github.com/klauspost/compress/s2
github.com/klauspost/compress/snappy
github.com/klauspost/compress/zstd
github.com/klauspost/compress/zstd/internal/xxhash
# github.com/olivere/ndjson v1.0.1
## explicit; go 1.16
github.com/olivere/ndjson
# github.com/pierrec/lz4/v4 v4.1.15
## explicit; go 1.14
github.com/pierrec/lz4/v4
github.com/pierrec/lz4/v4/internal/lz4block
github.com/pierrec/lz4/v4/internal/lz4errors
github.com/pierrec/lz4/v4/internal/lz4stream
github.com/pierrec/lz4/v4/internal/xxh32
# github.com/redis/go-redis/v9 v9.2.1
## explicit; go 1.18
github.com/redis/go-redis/v9
github.com/redis/go-redis/v9/internal
github.com/redis/go-redis/v9/internal/hashtag
github.com/redis/go-redis/v9/internal/hscan
github.com/redis/go-redis/v9/internal/pool
github.com/redis/go-redis/v9/internal/proto
github.com/redis/go-redis/v9/internal/rand
github.com/redis/go-redis/v9/internal/util
# github.com/russross/blackfriday/v2 v2.1.0
## explicit
github.com/russross/blackfriday/v2
# github.com/segmentio/kafka-go v0.4.44
## explicit; go 1.15
github.com/segmentio/kafka-go
github.com/segmentio/kafka-go/compress
github.com/segmentio/kafka-go/compress/gzip
github.com/segmentio/kafka-go/compress/lz4
github.com/segmentio/kafka-go/compress/snappy
github.com/segmentio/kafka-go/compress/zstd
github.com/segmentio/kafka-go/protocol
github.com/segmentio/kafka-go/protocol/addoffsetstotxn
github.com/segmentio/kafka-go/protocol/addpartitionstotxn
github.com/segmentio/kafka-go/protocol/alterclientquotas
github.com/segmentio/kafka-go/protocol/alterconfigs
github.com/segmentio/kafka-go/protocol/alterpartitionreassignments
github.com/segmentio/kafka-go/protocol/alteruserscramcredentials
github.com/segmentio/kafka-go/protocol/apiversions
github.com/segmentio/kafka-go/protocol/consumer
github.com/segmentio/kafka-go/protocol/createacls
github.com/segmentio/kafka-go/protocol/createpartitions
github.com/segmentio/kafka-go/protocol/createtopics
github.com/segmentio/kafka-go/protocol/deleteacls
github.com/segmentio/kafka-go/protocol/deletegroups
github.com/segmentio/kafka-go/protocol/deletetopics
github.com/segmentio/kafka-go/protocol/describeacls
github.com/segmentio/kafka-go/protocol/describeclientquotas
github.com/segmentio/kafka-go/protocol/describeconfigs
github.com/segmentio/kafka-go/protocol/describegroups
github.com/segmentio/kafka-go/protocol/describeuserscramcredentials
github.com/segmentio/kafka-go/protocol/electleaders
github.com/segmentio/kafka-go/protocol/endtxn
github.com/segmentio/kafka-go/protocol/fetch
github.com/segmentio/kafka-go/protocol/findcoordinator
github.com/segmentio/kafka-go/protocol/heartbeat
github.com/segmentio/kafka-go/protocol/incrementalalterconfigs
github.com/segmentio/kafka-go/protocol/initproducerid
github.com/segmentio/kafka-go/protocol/joingroup
github.com/segmentio/kafka-go/protocol/leavegroup
github.com/segmentio/kafka-go/protocol/listgroups
github.com/segmentio/kafka-go/protocol/listoffsets
github.com/segmentio/kafka-go/protocol/metadata
github.com/segmentio/kafka-go/protocol/offsetcommit
github.com/segmentio/kafka-go/protocol/offsetdelete
github.com/segmentio/kafka-go/protocol/offsetfetch
github.com/segmentio/kafka-go/protocol/produce
github.com/segmentio/kafka-go/protocol/saslauthenticate
github.com/segmentio/kafka-go/protocol/saslhandshake
github.com/segmentio/kafka-go/protocol/syncgroup
github.com/segmentio/kafka-go/protocol/txnoffsetcommit
github.com/segmentio/kafka-go/sasl
# github.com/urfave/cli v1.22.14
## explicit; go 1.11
github.com/urfave/cli
# golang.org/x/net v0.17.0
## explicit; go 1.17
golang.org/x/net/http/httpguts
golang.org/x/net/http2
golang.org/x/net/http2/hpack
golang.org/x/net/idna
golang.org/x/net/internal/timeseries
golang.org/x/net/trace
# golang.org/x/sys v0.13.0
## explicit; go 1.17
golang.org/x/sys/unix
# golang.org/x/text v0.13.0
## explicit; go 1.17
golang.org/x/text/secure/bidirule
golang.org/x/text/transform
golang.org/x/text/unicode/bidi
golang.org/x/text/unicode/norm
# google.golang.org/genproto/googleapis/rpc v0.0.0-20231012201019-e917dd12ba7a
## explicit; go 1.19
google.golang.org/genproto/googleapis/rpc/status
# google.golang.org/grpc v1.58.3
## explicit; go 1.19
google.golang.org/grpc
google.golang.org/grpc/attributes
google.golang.org/grpc/backoff
google.golang.org/grpc/balancer
google.golang.org/grpc/balancer/base
google.golang.org/grpc/balancer/grpclb/state
google.golang.org/grpc/balancer/roundrobin
google.golang.org/grpc/binarylog/grpc_binarylog_v1
google.golang.org/grpc/channelz
google.golang.org/grpc/codes
google.golang.org/grpc/connectivity
google.golang.org/grpc/credentials
google.golang.org/grpc/credentials/insecure
google.golang.org/grpc/encoding
google.golang.org/grpc/encoding/proto
google.golang.org/grpc/grpclog
google.golang.org/grpc/internal
google.golang.org/grpc/internal/backoff
google.golang.org/grpc/internal/balancer/gracefulswitch
google.golang.org/grpc/internal/balancerload
google.golang.org/grpc/internal/binarylog
google.golang.org/grpc/internal/buffer
google.golang.org/grpc/internal/channelz
google.golang.org/grpc/internal/credentials
google.golang.org/grpc/internal/envconfig
google.golang.org/grpc/internal/grpclog
google.golang.org/grpc/internal/grpcrand
google.golang.org/grpc/internal/grpcsync
google.golang.org/grpc/internal/grpcutil
google.golang.org/grpc/internal/idle
google.golang.org/grpc/internal/metadata
google.golang.org/grpc/internal/pretty
google.golang.org/grpc/internal/resolver
google.golang.org/grpc/internal/resolver/dns
google.golang.org/grpc/internal/resolver/passthrough
google.golang.org/grpc/internal/resolver/unix
google.golang.org/grpc/internal/serviceconfig
google.golang.org/grpc/internal/status
google.golang.org/grpc/internal/syscall
google.golang.org/grpc/internal/transport
google.golang.org/grpc/internal/transport/networktype
google.golang.org/grpc/keepalive
google.golang.org/grpc/metadata
google.golang.org/grpc/peer
google.golang.org/grpc/resolver
google.golang.org/grpc/serviceconfig
google.golang.org/grpc/stats
google.golang.org/grpc/status
google.golang.org/grpc/tap
# google.golang.org/protobuf v1.31.0
## explicit; go 1.11
google.golang.org/protobuf/encoding/protojson
google.golang.org/protobuf/encoding/prototext
google.golang.org/protobuf/encoding/protowire
google.golang.org/protobuf/internal/descfmt
google.golang.org/protobuf/internal/descopts
google.golang.org/protobuf/internal/detrand
google.golang.org/protobuf/internal/encoding/defval
google.golang.org/protobuf/internal/encoding/json
google.golang.org/protobuf/internal/encoding/messageset
google.golang.org/protobuf/internal/encoding/tag
google.golang.org/protobuf/internal/encoding/text
google.golang.org/protobuf/internal/errors
google.golang.org/protobuf/internal/filedesc
google.golang.org/protobuf/internal/filetype
google.golang.org/protobuf/internal/flags
google.golang.org/protobuf/internal/genid
google.golang.org/protobuf/internal/impl
google.golang.org/protobuf/internal/order
google.golang.org/protobuf/internal/pragma
google.golang.org/protobuf/internal/set
google.golang.org/protobuf/internal/strs
google.golang.org/protobuf/internal/version
google.golang.org/protobuf/proto
google.golang.org/protobuf/reflect/protodesc
google.golang.org/protobuf/reflect/protoreflect
google.golang.org/protobuf/reflect/protoregistry
google.golang.org/protobuf/runtime/protoiface
google.golang.org/protobuf/runtime/protoimpl
google.golang.org/protobuf/types/descriptorpb
google.golang.org/protobuf/types/known/anypb
google.golang.org/protobuf/types/known/durationpb
google.golang.org/protobuf/types/known/timestamppb
