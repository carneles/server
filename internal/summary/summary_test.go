package summary_test

import (
	"context"
	"errors"
	"reflect"
	"testing"

	"github.com/carneles/server/internal/summary"
	"github.com/go-redis/redismock/v9"
)

// TestGeneratePositive will test summary function using positive scenarios
func TestGeneratePositive(t *testing.T) {
	client, mock := redismock.NewClientMock()

	tests := []struct {
		desc       string
		expected   *summary.Summary
		redisValue []string
	}{
		{
			desc: "when redis returns value",
			expected: &summary.Summary{
				MeterId:    "ABCDEFG",
				TotalMeter: 380,
				TotalPrice: 38000,
				TotalPoint: 38000,
			},
			redisValue: []string{
				`{"meterId":"ABCDEFG","token":"123456","meter":120}`,
				`{"meterId":"ABCDEFG","token":"123457","meter":100}`,
				`{"meterId":"ABCDEFG","token":"123458","meter":110}`,
				`{"meterId":"ABCDEFG","token":"123459","meter":50}`,
			},
		},
		{
			desc: "when redis returns empty",
			expected: &summary.Summary{
				MeterId:    "ABCDEFG",
				TotalMeter: 0,
				TotalPrice: 0,
				TotalPoint: 0,
			},
			redisValue: []string{},
		},
		{
			desc: "when redis data cannot be parsed",
			expected: &summary.Summary{
				MeterId:    "ABCDEFG",
				TotalMeter: 0,
				TotalPrice: 0,
				TotalPoint: 0,
			},
			redisValue: []string{"some data", "just another data"},
		},
	}

	for _, tt := range tests {
		t.Run(tt.desc, func(t *testing.T) {
			generator := summary.NewSummaryGenerator(client)
			mock.ExpectLRange("ABCDEFG", 0, -1).SetVal(tt.redisValue)
			got, _ := generator.Generate(context.TODO(), &summary.Params{MeterId: "ABCDEFG"})
			if !reflect.DeepEqual(tt.expected, got) {
				t.Errorf(`Result %#v is not same with expectation %#v`, got, tt.expected)
			}
		})
	}
}

// TestGenerateNegative will test summary function using negative scenarios
func TestGenerateNegative(t *testing.T) {
	client, mock := redismock.NewClientMock()

	tests := []struct {
		desc        string
		expected    *summary.Summary
		expectedErr error
	}{
		{
			desc: "when redis returns error",
			expected: &summary.Summary{
				MeterId:    "ABCDEFG",
				TotalMeter: 0,
				TotalPrice: 0,
				TotalPoint: 0,
			},
			expectedErr: errors.New("some err"),
		},
	}

	for _, tt := range tests {
		t.Run(tt.desc, func(t *testing.T) {
			generator := summary.NewSummaryGenerator(client)
			mock.ExpectLRange("ABCDEFG", 0, -1).SetErr(tt.expectedErr)
			_, err := generator.Generate(context.TODO(), &summary.Params{MeterId: "ABCDEFG"})
			if !reflect.DeepEqual(tt.expectedErr, err) {
				t.Errorf(`Result %#v is not same with expectation %#v`, err, tt.expectedErr)
			}
		})
	}
}
