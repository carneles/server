package summary

import (
	"context"
	"encoding/json"

	"github.com/carneles/server/internal/changerecord"
	"github.com/redis/go-redis/v9"
)

type SummaryGenerator struct {
	client *redis.Client
}

func NewSummaryGenerator(redisClient *redis.Client) SummaryGeneratorServer {
	return SummaryGenerator{
		client: redisClient,
	}
}

// Generate is to generate summary
func (s SummaryGenerator) Generate(ctx context.Context, params *Params) (*Summary, error) {
	meterId := params.MeterId
	result := s.client.LRange(ctx, meterId, 0, -1)
	if result.Err() != nil {
		return nil, result.Err()
	}
	changeRecordsStr := result.Val()
	changeRecords := make(changerecord.ChangeRecords, 0)
	for _, changeRecordStr := range changeRecordsStr {
		var changeRecord changerecord.ChangeRecord
		if err := json.Unmarshal([]byte(changeRecordStr), &changeRecord); err == nil {
			changeRecords = append(changeRecords, changeRecord)
		}
	}
	if len(changeRecords) == 0 {
		return &Summary{
			MeterId:    meterId,
			TotalMeter: 0,
			TotalPrice: 0,
			TotalPoint: 0,
		}, nil
	}
	return &Summary{
		MeterId:    meterId,
		TotalMeter: changeRecords.TotalMeter(meterId),
		TotalPrice: changeRecords.TotalPrice(meterId),
		TotalPoint: changeRecords.TotalPoint(meterId),
	}, nil
}
