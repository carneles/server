package changerecord_test

import (
	"reflect"
	"testing"

	"github.com/carneles/server/internal/changerecord"
)

// TestCreateChangeRecord will test change record creation
func TestCreateChangeRecord(t *testing.T) {
	want := changerecord.ChangeRecord{
		MeterID: "ABCDEFG",
		Token:   "1234567",
		Meter:   10,
		Price:   100,
	}
	result := changerecord.NewChangeRecord("ABCDEFG", "1234567", 10, 100)
	if want != result {
		t.Errorf(`Result %+v is not same with expectation %+v`, result, want)
	}
}

// TestFilter is to test change records filter feature
func TestFilter(t *testing.T) {
	data := changerecord.ChangeRecords{
		changerecord.NewChangeRecord("ABCDEFG", "1234567", 10, 100),
		changerecord.NewChangeRecord("ABCDEFG", "1234568", 100, 1000),
		changerecord.NewChangeRecord("ABCDEFG", "1234569", 200, 2000),
		changerecord.NewChangeRecord("ABCDEFG", "1234560", 300, 3000),
	}

	tests := []struct {
		desc     string
		expected changerecord.ChangeRecords
		param    func(changerecord.ChangeRecord) bool
	}{
		{
			desc: "find meter greater than 10",
			expected: changerecord.ChangeRecords{
				changerecord.NewChangeRecord("ABCDEFG", "1234568", 100, 1000),
				changerecord.NewChangeRecord("ABCDEFG", "1234569", 200, 2000),
				changerecord.NewChangeRecord("ABCDEFG", "1234560", 300, 3000),
			},
			param: func(cr changerecord.ChangeRecord) bool {
				return cr.Meter > 10
			},
		},
		{
			desc: "find meter equal to 10",
			expected: changerecord.ChangeRecords{
				changerecord.NewChangeRecord("ABCDEFG", "1234567", 10, 100),
			},
			param: func(cr changerecord.ChangeRecord) bool {
				return cr.Meter == 10
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.desc, func(t *testing.T) {
			got := data.Filter(tt.param)
			if !reflect.DeepEqual(tt.expected, got) {
				t.Errorf(`Result %+v is not same with expectation %+v`, got, tt.expected)
			}
		})
	}
}

// TestTotalMeter is to test get total meter feature
func TestTotalMeter(t *testing.T) {
	tests := []struct {
		desc     string
		data     changerecord.ChangeRecords
		expected int64
	}{
		{
			desc: "find total meter - normal situation",
			data: changerecord.ChangeRecords{
				changerecord.NewChangeRecord("ABCDEFG", "1234567", 10, 100),
				changerecord.NewChangeRecord("ABCDEFG", "1234568", 100, 1000),
				changerecord.NewChangeRecord("ABCDEFG", "1234569", 200, 2000),
				changerecord.NewChangeRecord("ABCDEFG", "1234560", 300, 3000),
			},
			expected: 610,
		},
		{
			desc:     "find total meter - no record",
			data:     changerecord.ChangeRecords{},
			expected: 0,
		},
		{
			desc: "find total meter - no matching records",
			data: changerecord.ChangeRecords{
				changerecord.NewChangeRecord("ABCDEFH", "1234567", 10, 100),
				changerecord.NewChangeRecord("ABCDEFH", "1234568", 100, 1000),
				changerecord.NewChangeRecord("ABCDEFH", "1234569", 200, 2000),
				changerecord.NewChangeRecord("ABCDEFH", "1234560", 300, 3000),
			},
			expected: 0,
		},
	}

	for _, tt := range tests {
		t.Run(tt.desc, func(t *testing.T) {
			got := tt.data.TotalMeter("ABCDEFG")
			if !reflect.DeepEqual(tt.expected, got) {
				t.Errorf(`Result %+v is not same with expectation %+v`, got, tt.expected)
			}
		})
	}
}

// TestTotalPrice is to test get total price feature
func TestTotalPrice(t *testing.T) {
	tests := []struct {
		desc     string
		data     changerecord.ChangeRecords
		expected int64
	}{
		{
			desc: "find total price - normal situation",
			data: changerecord.ChangeRecords{
				changerecord.NewChangeRecord("ABCDEFG", "1234567", 10, 100),
				changerecord.NewChangeRecord("ABCDEFG", "1234568", 100, 1000),
				changerecord.NewChangeRecord("ABCDEFG", "1234569", 200, 2000),
				changerecord.NewChangeRecord("ABCDEFG", "1234560", 300, 3000),
			},
			expected: 6100,
		},
		{
			desc:     "find total price - no record",
			data:     changerecord.ChangeRecords{},
			expected: 0,
		},
		{
			desc: "find total price - no matching records",
			data: changerecord.ChangeRecords{
				changerecord.NewChangeRecord("ABCDEFH", "1234567", 10, 100),
				changerecord.NewChangeRecord("ABCDEFH", "1234568", 100, 1000),
				changerecord.NewChangeRecord("ABCDEFH", "1234569", 200, 2000),
				changerecord.NewChangeRecord("ABCDEFH", "1234560", 300, 3000),
			},
			expected: 0,
		},
	}

	for _, tt := range tests {
		t.Run(tt.desc, func(t *testing.T) {
			got := tt.data.TotalPrice("ABCDEFG")
			if !reflect.DeepEqual(tt.expected, got) {
				t.Errorf(`Result %+v is not same with expectation %+v`, got, tt.expected)
			}
		})
	}
}

// TestTotalPoint is to test get total point feature
func TestTotalPoint(t *testing.T) {
	tests := []struct {
		desc     string
		data     changerecord.ChangeRecords
		expected int64
	}{
		{
			desc: "find total point - normal situation",
			data: changerecord.ChangeRecords{
				changerecord.NewChangeRecord("ABCDEFG", "1234567", 10, 100),
				changerecord.NewChangeRecord("ABCDEFG", "1234568", 100, 1000),
				changerecord.NewChangeRecord("ABCDEFG", "1234569", 200, 2000),
				changerecord.NewChangeRecord("ABCDEFG", "1234560", 300, 3000),
			},
			expected: 6100,
		},
		{
			desc:     "find total point - no record",
			data:     changerecord.ChangeRecords{},
			expected: 0,
		},
		{
			desc: "find total point - no matching records",
			data: changerecord.ChangeRecords{
				changerecord.NewChangeRecord("ABCDEFH", "1234567", 10, 100),
				changerecord.NewChangeRecord("ABCDEFH", "1234568", 100, 1000),
				changerecord.NewChangeRecord("ABCDEFH", "1234569", 200, 2000),
				changerecord.NewChangeRecord("ABCDEFH", "1234560", 300, 3000),
			},
			expected: 0,
		},
	}

	for _, tt := range tests {
		t.Run(tt.desc, func(t *testing.T) {
			got := tt.data.TotalPoint("ABCDEFG")
			if !reflect.DeepEqual(tt.expected, got) {
				t.Errorf(`Result %+v is not same with expectation %+v`, got, tt.expected)
			}
		})
	}
}
