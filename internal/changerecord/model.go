package changerecord

import (
	"math"
	"os"
	"strconv"
)

// ChangeRecord is change record definition
type ChangeRecord struct {
	MeterID string `json:"meterId"`
	Token   string `json:"token"`
	Meter   int64  `json:"meter"`
	Price   int64  `json:"price"`
}

// NewChangeRecord is to create new change record
func NewChangeRecord(meterId, token string, meter, price int64) ChangeRecord {
	return ChangeRecord{
		MeterID: meterId,
		Token:   token,
		Meter:   meter,
		Price:   price,
	}
}

// ChangeRecords is collection of change record
type ChangeRecords []ChangeRecord

// Filter is to filter change record based on param
func (s ChangeRecords) Filter(f func(ChangeRecord) bool) (res ChangeRecords) {
	for _, v := range s {
		if f(v) {
			res = append(res, v)
		}
	}
	return
}

// TotalMeter is to get the total meter
func (s ChangeRecords) TotalMeter(meterId string) int64 {
	total := int64(0)
	// return early
	if len(s) == 0 {
		return total
	}
	accountableRecords := s.Filter(func(cr ChangeRecord) bool {
		return cr.MeterID == meterId
	})
	for _, cr := range accountableRecords {
		total += cr.Meter
	}

	return total
}

// TotalPrice is to get the total price
func (s ChangeRecords) TotalPrice(meterId string) int64 {
	total := int64(0)
	// return early
	if len(s) == 0 {
		return total
	}
	accountableRecords := s.Filter(func(cr ChangeRecord) bool {
		return cr.MeterID == meterId
	})
	for _, cr := range accountableRecords {
		total += cr.Price
	}

	return total
}

// TotalPoint is to get the totap point
func (s ChangeRecords) TotalPoint(meterId string) int64 {
	conversionValueStr := os.Getenv("CONVERSION_VALUE")
	conversionValue, err := strconv.ParseInt(conversionValueStr, 10, 64)
	if err != nil {
		conversionValue = 1
	}
	totalPrice := s.TotalPrice(meterId)

	return int64(math.Floor(float64(totalPrice) / float64(conversionValue)))
}
