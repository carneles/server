package worker_test

import (
	"reflect"
	"testing"

	"github.com/carneles/server/internal/changerecord"
	"github.com/carneles/server/internal/worker"
)

// TestToChangeRecord will test to change record func
func TestToChangeRecord(t *testing.T) {
	tests := []struct {
		desc     string
		data     worker.KafkaMessage
		expected changerecord.ChangeRecord
	}{
		{
			desc: "record",
			data: worker.KafkaMessage{
				MeterID: "1234567",
				Token:   "ABCDEFG",
				Meter:   "100",
				Price:   "1000",
			},
			expected: changerecord.ChangeRecord{
				MeterID: "1234567",
				Token:   "ABCDEFG",
				Meter:   100,
				Price:   1000,
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.desc, func(t *testing.T) {
			got := tt.data.ToChangeRecord()
			if !reflect.DeepEqual(tt.expected, got) {
				t.Errorf(`Result %#v is not same with expectation %#v`, got, tt.expected)
			}
		})
	}
}
