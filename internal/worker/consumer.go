package worker

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"strconv"
	"strings"

	"github.com/carneles/server/internal/changerecord"
	client "github.com/carneles/server/internal/pkg/redis"
	"github.com/redis/go-redis/v9"
	kafka "github.com/segmentio/kafka-go"
)

type Config struct {
	KafkaURL   string
	KafkaTopic string
	RedisURL   string
}

type Worker struct {
	config Config
	Redis  *redis.Client
}

type KafkaMessage struct {
	MeterID string `json:"meterID"`
	Token   string `json:"token"`
	Meter   string `json:"meter"`
	Price   string `json:"price"`
}

func (m KafkaMessage) ToChangeRecord() changerecord.ChangeRecord {
	meter, err := strconv.ParseInt(m.Meter, 10, 64)
	if err != nil {
		meter = int64(0)
	}
	price, err := strconv.ParseInt(m.Price, 10, 64)
	if err != nil {
		price = int64(0)
	}
	return changerecord.NewChangeRecord(m.MeterID, m.Token, meter, price)
}

func NewWorker(config Config) Worker {
	return Worker{
		config: config,
	}
}

func (c Worker) Start(ctx context.Context) {
	brokers := strings.Split(c.config.KafkaURL, ",")
	reader := kafka.NewReader(kafka.ReaderConfig{
		Brokers:  brokers,
		GroupID:  "server",
		Topic:    c.config.KafkaTopic,
		MaxBytes: 10e6,
	})

	redis := client.NewClient(client.Config{
		URL: c.config.RedisURL,
	})
	c.Redis = redis

	for {
		m, err := reader.ReadMessage(ctx)
		if err != nil {
			fmt.Printf("error read message from %s: %s \n", c.config.KafkaURL, err.Error())
			break
		}
		fmt.Printf("message at offset %d: %s = %s\n", m.Offset, string(m.Key), string(m.Value))
		c.process(ctx, m.Value)
	}

	if err := reader.Close(); err != nil {
		log.Fatal("failed to close reader:", err)
	}
}

func (c Worker) process(ctx context.Context, data []byte) {
	var message KafkaMessage

	err := json.Unmarshal(data, &message)
	if err != nil {
		fmt.Printf("Cannot process data %s: %s \n", string(data), err.Error())
		return
	}

	changeRecord := message.ToChangeRecord()
	jsonData, err := json.Marshal(changeRecord)
	if err != nil {
		jsonData = []byte(`{}`)
	}
	if err := c.Redis.RPush(ctx, changeRecord.MeterID, string(jsonData)).Err(); err != nil {
		fmt.Printf("Failed to store data to redis %s: %s \n", string(jsonData), err.Error())
	}
}
