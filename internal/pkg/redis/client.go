package redis

import (
	"github.com/redis/go-redis/v9"
)

type Config struct {
	URL string
}

func NewClient(config Config) *redis.Client {
	opts, err := redis.ParseURL(config.URL)
	if err != nil {
		panic(err)
	}

	redisClient := redis.NewClient(opts)

	return redisClient
}
