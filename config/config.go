package config

import (
	"os"
)

const (
	KafkaTopic = "server-topic"
	KafkaURL   = "localhost:9090,localhost:9091,localhost:9092"
	RedisURL   = "redis://0.0.0.0:6379"
	ServerPORT = ":9000"
	BasePrice  = "10"
)

func GetConfig(key string) string {
	value := os.Getenv(key)
	if value == "" {
		switch key {
		case "KAFKA_TOPIC":
			return KafkaTopic
		case "KAFKA_URL":
			return KafkaURL
		case "REDIS_URL":
			return RedisURL
		case "SERVER_PORT":
			return ServerPORT
		case "BASE_PRICE":
			return BasePrice
		default:
			return ""
		}
	}
	return value
}
