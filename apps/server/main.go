package main

import (
	"context"
	"log"
	"net"

	"github.com/carneles/server/config"
	"github.com/carneles/server/internal/pkg/redis"
	"github.com/carneles/server/internal/summary"
	"github.com/carneles/server/internal/worker"
	"google.golang.org/grpc"
)

func main() {
	srv := grpc.NewServer()
	redisClient := redis.NewClient(redis.Config{
		URL: config.GetConfig("REDIS_URL"),
	})
	summaryGeneratorSrv := summary.NewSummaryGenerator(redisClient)
	summary.RegisterSummaryGeneratorServer(srv, summaryGeneratorSrv)

	go func() {
		w := worker.NewWorker(worker.Config{
			KafkaURL:   config.GetConfig("KAFKA_URL"),
			KafkaTopic: config.GetConfig("KAFKA_TOPIC"),
			RedisURL:   config.GetConfig("REDIS_URL"),
		})
		w.Start(context.Background())
	}()

	listener, err := net.Listen("tcp", config.GetConfig("SERVER_PORT"))
	if err != nil {
		log.Fatalf("could not listen to %s: %v", config.GetConfig("SERVER_PORT"), err)
	}

	log.Fatal(srv.Serve(listener))
}
