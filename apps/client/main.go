package main

import (
	"context"
	"fmt"
	"log"
	"os"

	"github.com/carneles/server/config"
	"github.com/carneles/server/internal/summary"
	"github.com/urfave/cli"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func getSummaryByCode(meterId string) {
	conn, err := grpc.Dial(config.GetConfig("SERVER_PORT"), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		panic("cannot connect!")
	}
	summaryGenerator := summary.NewSummaryGeneratorClient(conn)
	summary, err := summaryGenerator.Generate(context.Background(), &summary.Params{
		MeterId: meterId,
	})
	if err != nil {
		fmt.Println("err: ", err)
		return
	}
	fmt.Printf("Summary of %s: \n", summary.MeterId)
	fmt.Println("- Total meter: ", summary.TotalMeter)
	fmt.Println("- Total price: ", summary.TotalPrice)
	fmt.Println("- Total point: ", summary.TotalPoint)
}

func main() {
	var token string
	app := &cli.App{
		Name:  "summary",
		Usage: "get summary of one meter",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:        "id",
				Usage:       "meter id to be checked",
				Destination: &token,
			},
		},
		Action: func(*cli.Context) error {
			getSummaryByCode(token)
			return nil
		},
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
